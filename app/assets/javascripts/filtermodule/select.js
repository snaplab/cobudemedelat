
// To make this work with turbolinks use:
// $(document).on('page:change', function(){
// However this still breaks with turbolinks
$(document).ready(function(){

	showSelects();
	submitSelect();
	selectRegionsInMap();
  checkForEmptyCheckboxes();

});


// function submitOnSelect(){
// 	var $select_form_radio_checkbox = $(".select-form input[type=radio], .select-form input[type=checkbox]"),
// 		$czech_map_area = $(".czech-map-area"),
// 		$date_from = $(".select-wrapper-date #date_from"),
// 		$date_to = $(".select-wrapper-date #date_to"),
// 		$region_reset = $('.select-regions label[for="region_reset"]');
//
// 	if($select_form_radio_checkbox.length){
// 		$select_form_radio_checkbox.click(function() {
// 		    $(this).closest("form").submit();
// 		});
// 	}
// 	if($czech_map_area.length){
// 		$czech_map_area.click(function(){
// 		    $(this).closest("form").submit();
// 		});
// 	}
// 	if($region_reset.length){
// 		$region_reset.click(function(){
// 			$(this).closest("form").submit();
// 		});
// 	}
// 	// if($date_from.length && $date_to.length){
// 	  // $date_from.on('change', function() {
// 	  // 	$(this).closest("form").submit();
// 	  // });
// 	// }
//
// }

function submitSelect(){
	var $submitSelect = $(".submitSelect");

	if($submitSelect.length){
		$submitSelect.click(function() {
		    $(this).closest("form").submit();
		});
	}

}

function checkboxCount(){
	var $select_form_checkbox = $(".select-form input[type=checkbox]"),
		$select_form_checkbox_checked = $(".select-form input[type=checkbox]:checked"),
		n = $select_form_checkbox_checked.length;
        return n;

}

function selectRegionsInMap(){
	var $czech_map_area = $(".czech-map-area"),
		checkedAny = 0;
	if($czech_map_area.length){
		for (var i = 0; i < 14; i++) {
			var region = "region_" + i;
			if($("#" + region + "").prop('checked')){
				checkedAny++;
				$(".czech-map-area[data-for=" + region + "]").attr('data-checked', 'true');
			}
		}
		 if(!checkedAny){
             $('.select-regions label[for="region_reset"]').addClass("region-no");
		 }
	}
}

function checkForEmptyCheckboxes(){
    if(!checkboxCount()) {
        $('.select-regions label[for="region_reset"]').addClass("region-no");
    } else {
        $('.select-regions label[for="region_reset"]').removeClass("region-no");
    }
}


function showSelects(){
	var $button_categories = $(".button-categories"),
		$button_regions =  $(".button-regions"),
		$button_date =  $(".button-date"),
		$select_categories = $(".select-categories"),
		$select_regions = $(".select-regions"),
		$select_date = $(".select-date"),
		$window_backdrop = $(".window-backdrop"),
		$categories_backdrop = $(".categories-backdrop"),
		$regions_backdrop = $(".regions-backdrop"),
		$date_backdrop = $(".date-backdrop");

	if($button_categories.length){
		$button_categories.click(function(){
			$select_categories.toggleClass("select-show");
			$button_categories.toggleClass("button-show");
			$select_regions.removeClass("select-show");
			$button_regions.removeClass("button-show");
			$select_date.removeClass("select-show");
			$button_date.removeClass("button-show");
			$window_backdrop.addClass("hide");
			$categories_backdrop.removeClass("hide");
		});
		$('.select-categories label[for="category_nil"]').click(function(){
			$(".select-categories input[type=checkbox]").prop('checked', false);
		});
	};
	if($button_regions.length){
		$button_regions.click(function(){
			$select_regions.toggleClass("select-show");
			$button_regions.toggleClass("button-show");
			$select_categories.removeClass("select-show");
			$button_categories.removeClass("button-show");
			$select_date.removeClass("select-show");
			$button_date.removeClass("button-show");
			$window_backdrop.addClass("hide");
			$regions_backdrop.removeClass("hide");
		});
		var $czech_map_area = $(".select-regions .czech-map-area");
		$(".select-regions input[type=checkbox] + label").hover(function(){
			var attrForOfLabel = $(this).attr("for");
			$(".czech-map-area[data-for=" + attrForOfLabel + "]").attr("class", "czech-map-area hover");
		}, function(){
			var attrForOfLabel = $(this).attr("for");
			$(".czech-map-area[data-for=" + attrForOfLabel + "]").attr("class", "czech-map-area");
		});
		$czech_map_area.hover(function(){
			var attrForOfArea = $(this).data("for");
			$(".select-regions input[type=checkbox] + label[for=" + attrForOfArea + "]").toggleClass("hover");
		});
		$czech_map_area.click(function(){
			var $this = $(this),
				attrForOfArea = $(this).data("for"),
				$input_checkbox = $(".select-regions input[type=checkbox]#" + attrForOfArea + "");
			if($input_checkbox.prop('checked')){
				$input_checkbox.prop('checked', false);
				$this.attr('data-checked', 'false');
			} else {
				$input_checkbox.prop('checked', true);
				$this.attr('data-checked', 'true');
			}
		});
		$(".select-regions input[type=checkbox] + label").click(function(){
			var attrForOfLabel = $(this).attr("for"),
				$czech_map_area_label = $(".czech-map-area[data-for=" + attrForOfLabel + "]");
			if($(".select-regions input[type=checkbox]#" + attrForOfLabel + "").prop('checked')){
                $czech_map_area_label.attr('data-checked', 'false');
            } else {
                $czech_map_area_label.attr('data-checked', 'true');
            }
        });
		$('.select-regions label[for="region_reset"]').click(function(){
			var attrForOfLabel = $(this).attr("for");
			$czech_map_area.attr('data-checked', 'false');
			$(".select-regions input[type=checkbox]").prop('checked', false);
		});
	};
	if($button_date.length){
		$button_date.click(function(){
			$select_date.toggleClass("select-show");
			$button_date.toggleClass("button-show");
			$select_regions.removeClass("select-show");
			$button_regions.removeClass("button-show");
			$select_categories.removeClass("select-show");
			$button_categories.removeClass("button-show");
			$window_backdrop.addClass("hide");
			$date_backdrop.removeClass("hide");
		});
	};
	$window_backdrop.click(function() {
			$select_regions.removeClass("select-show");
			$button_regions.removeClass("button-show");
			$select_categories.removeClass("select-show");
			$button_categories.removeClass("button-show");
			$select_date.removeClass("select-show");
			$button_date.removeClass("button-show");
			$window_backdrop.addClass("hide");
	});
	$('.cancelSelect').click(function() {
			$select_regions.removeClass("select-show");
			$button_regions.removeClass("button-show");
			$select_categories.removeClass("select-show");
			$button_categories.removeClass("button-show");
			$select_date.removeClass("select-show");
			$button_date.removeClass("button-show");
			$window_backdrop.addClass("hide");
	});
	$button_categories.click(function(event){
	    event.stopPropagation();
	});
	$button_regions.click(function(event){
	    event.stopPropagation();
	});
	$button_date.click(function(event){
	    event.stopPropagation();
	});
	$select_categories.click(function(event){
	    event.stopPropagation();
	});
	$select_regions.click(function(event){
	    event.stopPropagation();
        checkForEmptyCheckboxes();
	});
}
