# == Schema Information
#
# Table name: venues
#
#  id                    :integer          not null, primary key
#  name                  :string
#  street                :string
#  city                  :string
#  zipcode               :string
#  phone_number          :string
#  agree_with_conditions :boolean
#  description           :string
#  business_hours        :string
#  website               :string
#  contact_email         :string
#  gps_lat               :string
#  gps_lon               :string
#  owner_id              :integer
#  region_id             :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

FactoryGirl.define do
  factory :venue do
    name "MyString"
    street "MyString"
    city "MyString"
    zipcode "MyString"
    phone_number "MyString"
    agree_with_conditions false
    description "MyString"
    business_hours "MyString"
    website "MyString"
    contact_email "MyString"
    gps_lat "MyString"
    gps_lon "MyString"
  end
end
