$(document).ready(function(){

	showCompanyPastEvents();

});

function showCompanyPastEvents(){

	var $pastEventsButton = $(".pastEventsButton"),
		$companyPastEvents = $(".companyPastEvents");

	if($pastEventsButton.length){
		$pastEventsButton.click(function(){
			$(this).toggleClass("button-show");
			$companyPastEvents.toggleClass("hide");
		});
	};

}
