Instructions:
================
* Have at least `ruby 2.3.0` installed (prefer installing with RVM)
* Have PostgreSQL installed and running
* Rename `database.example.yml` to `database.yml`
* Fix database credentials to yours
* Rename `secrets.example.yml` to `secrets.yml`

* `bundle install` Bundle gemfile
* `rake db:create` Create database
* `rake db:migrate` Run database migrations
* `rake db:seed` Seed the database
* `rails s` Run the rails server
* Browse to `http://localhost:3000`
* Test user and admin user credentials below:

**User**
```
email: testuser@gmail.com
password: letmein123
```
**Admin**
```
email: testadmin@gmail.com
password: letmein123
```
