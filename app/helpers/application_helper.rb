module ApplicationHelper
  def get_date_label_value
    if !params[:date_from].blank? && !params[:date_to].blank?
      # We have from-to params
      if params[:date_from] == params[:date_to]
        # Params are equal
        if params[:date_to].to_date == Date.today
          # Today
          "Dnes #{params[:date_from]}"
        elsif params[:date_to].to_date == Date.today+1
          "Zítra #{params[:date_from]}"
        else
          "#{params[:date_from]}"
        end
      else
        "#{params[:date_from]} - #{params[:date_to]}"
      end
    else
      "Dnes #{I18n.l Date.today, format: :long}"
    end
  end
end
