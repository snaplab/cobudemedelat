json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :venue_id, :start_time, :end_time, :organizer_id, :category_id, :slug, :position, :accepted
  json.url event_url(event, format: :json)
end
