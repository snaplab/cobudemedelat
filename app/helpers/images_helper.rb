# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  link       :string           default("")
#  primary    :boolean
#  caption    :string           default("")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module ImagesHelper
end
