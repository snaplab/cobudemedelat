json.array!(@venues) do |venue|
  json.extract! venue, :id, :name, :street, :city, :zipcode, :phone_number, :agree_with_conditions, :description, :business_hours, :website, :contact_email, :gps_lat, :gps_lon
  json.url venue_url(venue, format: :json)
end
