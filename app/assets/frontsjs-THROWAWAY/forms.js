
$(document).ready(function(){

	selectUserCompany();
	choosePermanentEvent();
});

function selectUserCompany(){
	var $select_user = $("input[id='user_account_type_users']"),
		$select_company = $("input[id='user_account_type_companies']"),
		$hideable = $(".hideable"),
		$hideable_2 = $(".hideable-2"),
		$nullable = $(".nullable");

	if($select_user.length){
		$select_user.click(function() {
			$hideable.addClass("hide");
			$hideable_2.removeClass("hide");
			$nullable.val('');
		});
	}
	if($select_company.length){
		$select_company.click(function() {
			$hideable.removeClass("hide");
			$hideable_2.addClass("hide");
		});
	}
}

function choosePermanentEvent(){
	var $choose_permanent = $("input[id='event_permanent']"),
		$hideable = $(".hideable"),
		$nullable = $(".nullable");

	if($choose_permanent.length){
		$choose_permanent.click(function() {
			if (!$hideable.hasClass('hide')){
				$hideable.addClass("hide");
				$nullable.val('');
			} else {
				$hideable.removeClass("hide");
				$nullable.val('');				
			}
		});
	}
}

// function checkboxCount(){
// 	var $select_form_checkbox = $(".select-form input[type=checkbox]"),
// 		$select_form_checkbox_checked = $(".select-form input[type=checkbox]:checked"),
// 		n = $select_form_checkbox_checked.length;

// 	// if($select_form_checkbox.length){
// 	// 	if(n == 1){
			
// 	// 	} else if(n < 1){
// 	// 		bb
// 	// 	}
// 	// }
// }

// function showSelects(){
// 	var $button_categories = $(".button-categories"),
// 		$button_regions =  $(".button-regions"),
// 		$select_categories = $(".select-categories"),
// 		$select_regions = $(".select-regions");

// 	if($button_categories.length){
// 		$button_categories.click(function(){
// 			$select_categories.toggleClass("select-show");
// 			$button_categories.toggleClass("button-show");
// 			$select_regions.removeClass("select-show");
// 			$button_regions.removeClass("button-show");
// 		});
// 	};
// 	if($button_regions.length){
// 		$button_regions.click(function(){
// 			$select_regions.toggleClass("select-show");
// 			$button_regions.toggleClass("button-show");
// 			$select_categories.removeClass("select-show");
// 			$button_categories.removeClass("button-show");
// 		});
// 		$(".select-form input[type=checkbox] + label").hover(function(){
// 			var attrForOfLabel = $(this).attr("for");
// 			console
// 			$(".select-form input[type=checkbox] + label[for=" + attrForOfLabel + "]").toggleClass("hover");
// 		});
// 	};
// 	$('html').click(function() {
// 			$select_regions.removeClass("select-show");
// 			$button_regions.removeClass("button-show");
// 			$select_categories.removeClass("select-show");
// 			$button_categories.removeClass("button-show");
// 	});
// 	$button_categories.click(function(event){
// 	    event.stopPropagation();
// 	});
// 	$button_regions.click(function(event){
// 	    event.stopPropagation();
// 	});
// }