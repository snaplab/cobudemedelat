class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :zipcode
      t.string :phone_number
      t.boolean :agree_with_conditions
      t.string :description
      t.string :business_hours
      t.string :website
      t.string :contact_email
      t.string :gps_lat
      t.string :gps_lon
      t.integer :owner_id
      t.integer :region_id

      t.timestamps null: false
    end
  end
end
