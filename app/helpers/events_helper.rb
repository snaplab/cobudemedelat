# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  title        :string
#  description  :text             default("")
#  start_time   :datetime
#  end_time     :datetime
#  venue_id     :integer
#  category_id  :integer
#  organizer_id :integer
#  slug         :string
#  position     :integer
#  accepted     :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module EventsHelper
end
