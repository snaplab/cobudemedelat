class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :parent_id
      t.string :name
      t.string :slug
      t.integer :position

      t.timestamps null: false
    end
  end
end
