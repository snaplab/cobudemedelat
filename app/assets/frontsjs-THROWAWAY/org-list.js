$(document).ready(function(){

	openListItem();

});

function openListItem(){

	var $cat_card_head = $(".cat-card-head");

	if($cat_card_head.length){
		$cat_card_head.click(function(){
			var $this = $(this);
			if($this.parent().children("ul").hasClass("hide")){
				$cat_card_head.parent().children("ul").addClass("hide");
				$cat_card_head.children(".closeCat").addClass("hide");
				$cat_card_head.removeClass("clicked");
				$this.parent().children("ul").removeClass("hide");
				$this.children(".closeCat").removeClass("hide");
				$this.addClass("clicked");
			} else {
				$this.parent().children("ul").addClass("hide");
				$this.children(".closeCat").addClass("hide");
				$this.removeClass("clicked");
			};
		});
		$cat_card_head.children(".closeCat").click(function(){
			$this.addClass("hide");
			$this.parent().parent().children("ul").addClass("hide");
			$this.removeClass("clicked");
		});
	};

}
