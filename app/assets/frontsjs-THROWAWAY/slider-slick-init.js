$(document).ready(function(){

  bannerBigSlider();
  // bannerSearchSlider();
  // bannerSideRightSlider();
  // eventDetailSlider();

});

$(window).load(function(){
  eventDetailSlider();
  companyDetailSlider();
});

function bannerBigSlider(){
  var $banner_big = $('.banner-big');
  if($banner_big.length){
    $banner_big.slick({
      arrows: false,
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true,
      centerPadding: "0px"
    });
  }
}

// function bannerSearchSlider(){
//   var $banner_search = $('.banners-search');
//   if($banner_search.length){
//     $banner_search.slick({
//       arrows: false,
//       dots: false,
//       infinite: true,
//       speed: 300,
//       slidesToShow: 1,
//       adaptiveHeight: true,
//       centerPadding: "0px"
//     });
//   }
// }

// function bannerSideRightSlider(){
//   var $banner = $('.banners-side-right');
//   if($banner.length){
//     $banner.slick({
//       arrows: false,
//       dots: false,
//       infinite: true,
//       speed: 300,
//       slidesToShow: 1,
//       adaptiveHeight: true,
//       centerPadding: "0px"
//     });
//   }
// }

function companyDetailSlider(){

var $event_detail_for = $('.event-detail-for'),
    $event_detail_nav = $('.event-detail-nav');

  if($event_detail_for.length){
    $event_detail_for.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      asNavFor: '.event-detail-nav'
    });
    if($event_detail_nav.length){
      $event_detail_nav.slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.event-detail-for',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        vertical: true,
        swipe: false,
        prevArrow: '',
        nextArrow: '<div class="event-slide-next"></div>',
        responsive: [
          // {
          //   breakpoint: 1281,
          //   settings: {
          //     vertical: false,
          //     swipe: true,
          //   }
          // },
          // {
          //   breakpoint: 1025,
          //   settings: {
          //     vertical: true,
          //     swipe: false,
          //   }
          // },
          {
            breakpoint: 821,
            settings: {
              vertical: false,
              swipe: true,
            }
          }
        ]
      });
    }
  }

}

function eventDetailSlider(){

var $company_detail_for = $('.company-detail-for');

  if($company_detail_for.length){
    $company_detail_for.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      infinite: true,
      speed: 300,
      autoplay: true
    });
  }

}