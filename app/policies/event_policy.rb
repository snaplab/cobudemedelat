class EventPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @event = model
  end

  def index?
    true
  end

  def show?
    true
  end

  def update?
    @current_user.admin? or @event.organizer == @current_user
  end

  def create?
    @current_user.admin? or @current_user
  end

  def edit?
    @current_user.admin? or @event.organizer == @current_user
  end

  def destroy?
    @current_user.admin? or @event.organizer == @current_user
  end

end
