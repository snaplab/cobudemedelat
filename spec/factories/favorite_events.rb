# == Schema Information
#
# Table name: favorite_events
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  event_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :favorite_event do
    user_id 1
    event_id 1
  end
end
