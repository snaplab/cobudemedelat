json.array!(@favorite_events) do |favorite_event|
  json.extract! favorite_event, :id, :user_id, :event_id
  json.url favorite_event_url(favorite_event, format: :json)
end
