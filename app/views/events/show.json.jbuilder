json.extract! @event, :id, :title, :description, :start_time, :end_time, :organizer_id, :category_id, :region_id, :slug, :position, :accepted, :created_at, :updated_at
