# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  title        :string
#  description  :text             default("")
#  start_time   :datetime
#  end_time     :datetime
#  venue_id     :integer
#  category_id  :integer
#  organizer_id :integer
#  slug         :string
#  position     :integer
#  accepted     :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index

    @events = Event.all.page(params[:page])

    # TODO Refactor this mess!!!!!!! Maybe use some gem? Maybe create Search model?

    # Dates
    @events = Event.within_dates(Time.parse(params[:start_time]), Time.parse(params[:end_time]))
                   .page(params[:page]) if params[:start_time].present? && params[:end_time].present?

    # Regions
    @events = Event.in_regions(params[:region])
                   .page(params[:page]) if params[:region].present?

    # Categories
    @events = Event.in_categories(params[:category])
                   .page(params[:page]) if params[:category].present?

    # Regions && Categories
    @events = Event.in_categories(params[:category])
                   .in_regions(params[:region])
                   .page(params[:page]) if params[:category].present? && params[:region].present?

    # Dates && Regions
    @events = Event.within_dates(Time.parse(params[:start_time]), Time.parse(params[:end_time]))
                   .in_regions(params[:region])
                   .page(params[:page]) if params[:region].present? && params[:start_time].present? && params[:end_time].present?

    # Dates && Categories
    @events = Event.in_categories(params[:category])
                   .within_dates(Time.parse(params[:start_time]), Time.parse(params[:end_time]))
                   .page(params[:page]) if params[:category].present? && params[:start_time].present? && params[:end_time].present?

    # Dates && Regions && Categories
    @events = Event.within_dates(Time.parse(params[:start_time]), Time.parse(params[:end_time]))
                   .in_regions(params[:region])
                   .in_categories(params[:category])
                   .page(params[:page]) if params[:category].present? && params[:region].present? && params[:start_time].present? && params[:end_time].present?

  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
    authorize @event
  end

  # POST /events
  # POST /events.json
  def create
    authorize Event
    if current_user
      @event = current_user.events.new(event_params)
    else
      @event = Event.new(event_params)
    end


    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    authorize @event

    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    authorize @event

    @event.destroy
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :start_time, :end_time, :venue_id, :category_id, :region_id, :slug, :position, :accepted)
    end
end
