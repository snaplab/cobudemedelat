//
//
// Komponenta a jeji API je zde:
// http://eonasdan.github.io/bootstrap-datetimepicker/#min-view-mode
//
// neco se da okoukat taky tady, ale to neni origo pro toto API:
// https://github.com/zpaulovics/datetimepicker-rails
// http://www.malot.fr/bootstrap-datetimepicker/
//
//
//

$(document).on('ready page:change', function() {
  var start_picker = $('#datetimepicker-event-start').datetimepicker({
    // inline: true
    // put here your custom picker options, that should be applied for all pickers
    stepping: 15,
    minDate: new Date((new Date().getFullYear()), (new Date().getMonth()), (new Date().getDate()), new Date().getHours() + 1 ),
    maxDate: new Date((new Date().getFullYear() + 2), (new Date().getMonth()), (new Date().getDate()) ),
    // locale: 'cs',
    // sideBySide: false,
    showTodayButton: true
  });

  var end_picker = $('#datetimepicker-event-end').datetimepicker({
    // inline: true
    // put here your custom picker options, that should be applied for all pickers
    stepping: 15,
    useCurrent: false,
    minDate: new Date((new Date().getFullYear()), (new Date().getMonth()), (new Date().getDate()) ),
    maxDate: new Date((new Date().getFullYear() + 2), (new Date().getMonth()), (new Date().getDate()) )
    // locale: 'cs',
    // sideBySide: false,
    // showTodayButton: true
  });

  start_picker.on("dp.change", function (e) {
      end_picker.data("DateTimePicker").minDate(e.date);
  });

  end_picker.on("dp.change", function (e) {
      start_picker.data("DateTimePicker").maxDate(e.date);
  });


    var $selectbutton = $('.select-wrapper-date .select-button'),
      $daterangepicker = $('.datetimerange')

  // $selectbutton.html(moment().subtract(29, 'days').format('DD.MM.YYYY') + ' - ' + moment().format('DD.MM.YYYY'));

  $daterangepicker.daterangepicker({
        format: 'DD.MM.YYYY',
        // startDate: moment().subtract(29, 'days'),
        // endDate: moment(),
        // minDate: '01/01/2012',
        // maxDate: '31/12/2015',
        // dateLimit: { days: 60 },
        showDropdowns: false,
        showWeekNumbers: false,
        timePicker: false,
        ranges: {
           'Dnes': [moment(), moment()],
           'Zítra': [moment().add(1, 'day'), moment().add(1, 'day')],
           'Pozítří': [moment().add(2, 'day'), moment().add(2, 'day')],
           'Tento víkend': [moment().endOf('week').subtract(1, 'days'), moment().endOf('week')],
           'Tento týden': [moment().startOf('week'), moment().endOf('week')]
           //'Tento měsíc': [moment().startOf('month'), moment().endOf('month')]
           // 'Příští víkend': [moment().add(1, 'week').endOf('week').subtract(1, 'days'), moment().add(1, 'week').endOf('week')],
           // 'Příští týden': [moment().add(1, 'week').startOf('week'), moment().add(1, 'week').endOf('week')],
           // 'Příští měsíc': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')]
           // 'Včera': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           // 'Minulých 7 dní': [moment().subtract(6, 'days'), moment()],
           // 'Minulých 30 dní': [moment().subtract(29, 'days'), moment()],
           // 'Minulý měsíc': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: '',
        cancelClass: 'btn-default',
        separator: ' až ',
        locale: {
            applyLabel: 'Potvrdit',
            cancelLabel: 'Zrušit',
            fromLabel: 'Od',
            toLabel: 'Do',
            customRangeLabel: 'Vlastní',
            // daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $selectbutton.html(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
  });

  $daterangepicker.on('apply.daterangepicker', function(ev, picker) {
    console.log(picker.startDate.format('DD.MM.YYYY'));
    $('#start_time').val(picker.startDate.format('DD.MM.YYYY'));
    console.log(picker.endDate.format('DD.MM.YYYY'));
    $('#end_time').val(picker.endDate.format('DD.MM.YYYY'));
    $(this).closest("form").submit();
  });

  $daterangepicker.on('cancel.daterangepicker', function(ev, picker) {
    //do something, like clearing an input
    $('#start_time').val('');
    $('#end_time').val('');
    $(this).closest("form").submit();
  });

  // $('.datepicker-date-from').datetimepicker({
  //   // inline: true
  //   // put here your custom picker options, that should be applied for all pickers
  //   // locale: 'cs',
  //   inline: true,
  //   sideBySide: false
  // });

  // $('.datepicker-date-to').datetimepicker({
  //   // inline: true
  //   // put here your custom picker options, that should be applied for all pickers
  //   // locale: 'cs',
  //   inline: true,
  //   sideBySide: false
  // });


  // $('.datetimerange').each(function(){
  //   var $this = $(this)
  //   var range1 = $($this.find('.input-group')[0])
  //   var range2 = $($this.find('.input-group')[1])

  //   if(range1.data("DateTimePicker").date() != null)
  //     range2.data("DateTimePicker").minDate(range1.data("DateTimePicker").date());

  //   if(range2.data("DateTimePicker").date() != null)
  //     range1.data("DateTimePicker").maxDate(range2.data("DateTimePicker").date());

  //   range1.on("dp.change",function (e) {
  //     if(e.date)
  //       range2.data("DateTimePicker").minDate(e.date);
  //     else
  //       range2.data("DateTimePicker").minDate(false);
  //   });

  //   range2.on("dp.change",function (e) {
  //     if(e.date)
  //       range1.data("DateTimePicker").maxDate(e.date);
  //     else
  //       range1.data("DateTimePicker").maxDate(false);
  //   });
  // })
});
