$(document).ready(function(){

	mySelectionSwitch();
  updateFavoriteItemsAJAX();

});

function mySelectionSwitch(){
	var $my_selection_future = $(".my-selection-future"),
		$my_selection_past = $(".my-selection-past"),
		$my_selection_future_content = $(".my-selection-future-content"),
		$my_selection_past_content = $(".my-selection-past-content"),
		$my_selection_popup = $("#my-selection-popup");

	if($my_selection_future.length){
		$my_selection_future.click(function(){
			$(this).addClass("selected");
			$my_selection_past.removeClass("selected");
			$my_selection_future_content.removeClass("hide");
			$my_selection_past_content.addClass("hide");
		});
	};
	if($my_selection_past.length){
		$my_selection_past.click(function(){
			$(this).addClass("selected");
			$my_selection_future.removeClass("selected");
			$my_selection_past_content.removeClass("hide");
			$my_selection_future_content.addClass("hide");
		});
	};
	if($my_selection_popup.length){
		$my_selection_popup.on('hidden.bs.modal', function () {
			$my_selection_future.addClass("selected");
			$my_selection_past.removeClass("selected");
			$my_selection_future_content.removeClass("hide");
			$my_selection_past_content.addClass("hide");
		});
	};
}

function updateFavoriteItemsAJAX(){
    $('.remove-favorite-event').bind('ajax:success', function() {
        console.log(1);
        $(this).closest('.event-card').fadeOut();
    });
}
