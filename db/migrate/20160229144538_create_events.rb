class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description, default: ""
      t.datetime :start_time
      t.datetime :end_time
      t.integer :venue_id
      t.integer :category_id
      t.integer :organizer_id
      t.string :slug
      t.integer :position
      t.boolean :accepted

      t.timestamps null: false
    end
  end
end
