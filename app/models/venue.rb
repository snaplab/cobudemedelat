# == Schema Information
#
# Table name: venues
#
#  id                    :integer          not null, primary key
#  name                  :string
#  street                :string
#  city                  :string
#  zipcode               :string
#  phone_number          :string
#  agree_with_conditions :boolean
#  description           :string
#  business_hours        :string
#  website               :string
#  contact_email         :string
#  gps_lat               :string
#  gps_lon               :string
#  owner_id              :integer
#  region_id             :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Venue < ActiveRecord::Base
  has_many :events
  belongs_to :owner, :class_name => "User"
  belongs_to :region
end
