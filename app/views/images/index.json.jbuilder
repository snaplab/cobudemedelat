json.array!(@images) do |image|
  json.extract! image, :id, :event_id, :link, :primary, :caption
end
