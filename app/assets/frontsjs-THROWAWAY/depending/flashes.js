
function flashes(scope){
  var $obj = $('.flash');
  if (scope) $obj = $('.flash',scope);
    
    $obj.each(function(){
      var $flash = $(this);
      var $close = $('<span class="close">&#215;</span>');
      $close.click(function(){
        $(this).parents('.flash').fadeOut(400,function(){
          $(this).remove();
        });
      });

    if ($(this).find('.wrapper').length){
      $(this).find('.wrapper').append($close);
    }
    else if ($(this).find('.pagewidth').length){
     $(this).find('.pagewidth').append($close);
    }
    else{
     $(this).append($close);
    } 
    setTimeout(function(){
      if ($flash.length){
        $flash.fadeOut(600,function(){
          $flash.remove();
        });
      } 
    },4000);
  });
}
$(function(){
  flashes();
}); 