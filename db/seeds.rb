Region.create({id: 0, name: "Praha", slug: "praha"})
Region.create({id: 1, name: "Jihocesky", slug: "jihocesky"})
Region.create({id: 2, name: "Jihomoravsky", slug: "jihomoravsky"})
Region.create({id: 3, name: "Karlovarsky", slug: "karlovarsky"})
Region.create({id: 4, name: "Kralovehradecky", slug: "kralovehradecky"})
Region.create({id: 5, name: "Liberecky", slug: "liberecky"})
Region.create({id: 6, name: "Moravskoslezsky", slug: "moravskoslezsky"})
Region.create({id: 7, name: "Olomoucky", slug: "olomoucky"})
Region.create({id: 8, name: "Pardubicky", slug: "pardubicky"})
Region.create({id: 9, name: "Plzensky", slug: "plzensky"})
Region.create({id: 10, name: "Stredocesky", slug: "stredocesky"})
Region.create({id: 11, name: "Ustecky", slug: "ustecky"})
Region.create({id: 12, name: "Vysocina", slug: "vysocina"})
Region.create({id: 13, name: "Zlinsky", slug: "zlinsky"})

u1 = User.new({email: "testadmin@gmail.com", password: "letmein123", name: "David C", role: 2})
u2 = User.new({email: "testuser@gmail.com", password: "letmein123", name: "Test User", role: 0})
u1.skip_confirmation!
u2.skip_confirmation!
u1.save!
u2.save!


20.times do |i|
  User.create({
    email: Faker::Internet.email,
    password: "letmein123",
    name: Faker::Name.name,
    role: 0
    })
  puts "create user #{i}"
end


categories = ['Food', 'Kids', 'Music', 'Sports', 'Nightlife', 'Gardening', 'Travel', 'Business', 'Cars']

categories.each do |c, i|
  Category.create({name: c, position: i, slug: c.downcase})
  puts "create category #{i}"
end


50.times do |i|
  Venue.create({
    name: Faker::Company.name,
    street: Faker::Address.street_address,
    city: Faker::Address.city,
    zipcode: Faker::Address.zip_code,
    phone_number: Faker::PhoneNumber.phone_number,
    agree_with_conditions: true,
    description: Faker::Hipster.sentence,
    website: Faker::Internet.domain_name,
    contact_email: Faker::Internet.email,
    gps_lat: Faker::Address.latitude,
    gps_lon: Faker::Address.longitude,
    owner: User.all.sample,
    region: Region.all.sample
    })

    puts "create venue #{i}"
end


100.times do |i|
  e = Event.new({
    title: Faker::Company.catch_phrase,
    description: Faker::Hipster.paragraph(2),
    start_time: Faker::Time.between(2.days.ago, Time.now, :all),
    end_time: Faker::Time.between(2.days.ago, Time.now, :all),
    venue: Venue.all.sample,
    category: Category.all.sample,
    accepted: [1,0].sample,
    organizer: User.all.sample
    })

  e.images.new({link: "https://unsplash.it/200/300/?random"})

  e.save!

  puts "create event #{i}"
end
