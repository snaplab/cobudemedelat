# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  title        :string
#  description  :text             default("")
#  start_time   :datetime
#  end_time     :datetime
#  venue_id     :integer
#  category_id  :integer
#  organizer_id :integer
#  slug         :string
#  position     :integer
#  accepted     :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :event do
    title "MyString"
    description "MyText"
    date_from "2016-02-29 15:45:38"
    date_to "2016-02-29 15:45:38"
    organizer_id 1
    category_id 1
    region_id 1
    slug "MyString"
    position 1
    accepted false
  end
end
