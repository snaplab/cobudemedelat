function autocomplete_main(){
  var $form = $("#main_search_form");
  if (!$form.length) return;
  var $input = $form.find("input#text");
  $input.autocomplete({
    minLength: 3,
    source: $input.data("autocompletepath"),
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      var newText = String(item.title).replace(
          new RegExp(this.term, "gi"),
          "<span class='ui-state-highlight'>$&</span>");
    return $( "<li>" )
    .append( "<a href='"+ item.url +"'>" + newText + "<i>"+ " " +"</i>" + "</a>" )
    .appendTo( ul );
  };
  $input.autocomplete( "instance" )._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth()+40);
  }


}
function monkeyPatchAutocomplete() {

    // don't really need this, but in case I did, I could store it and chain
    var oldFn = $.ui.autocomplete.prototype._renderItem;

    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("^" + this.term) ;
        var t = item.label.replace(re,"<span style='font-weight:bold;color:Blue;'>" +
            this.term +
            "</span>");
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}


$(window).on("load", autocomplete_main);
$(window).on("load", monkeyPatchAutocomplete);