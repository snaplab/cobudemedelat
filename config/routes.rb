Rails.application.routes.draw do

  get 'set_language/english'
  get 'set_language/czech'

  # namespace :admin do
  #   resources :users
  #   root to: "users#index"
  # end

  devise_for :users

  resources :users
  resources :venues
  resources :regions
  resources :categories
  resources :events do
    resources :images
  end
  resources :events
  resources :favorite_events, path: "/favorite"
  root to: 'events#index'

end
