// This wont work with turbolinks.
// Need to user $(document).on('ready page:change', function(){ ...
$(document).ready(function(){

	menuOpener();
	mySelectionOpener();

});

function menuOpener(){

var $menu = $("#menu"),
	$link_6 = $(".link_6"),
	$my_selection_popup = $("#my-selection-popup");

	if($menu.length){
		$menu.click(function(){
			$(this).toggleClass("clicked");
		});
		$('html').click(function() {
			$menu.removeClass("clicked");
		});
		$menu.click(function(event){
		    event.stopPropagation();
		});
	}
}

function mySelectionOpener(){

var $link_6 = $(".link_6"),
	$my_selection_popup = $("#modal-wrapper #my-selection-popup");

	if($link_6.length){
		$link_6.click(function(event){
		    event.preventDefault();
		});
		if($my_selection_popup.length){
			$link_6.click(function(){
			    $my_selection_popup.modal();

                $my_selection_popup.on('shown.bs.modal', function() {
                    $('#menu li').removeClass('selected');
                    $link_6.parent().addClass('selected');
                })
			});
		}
	}
}
