class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :event_id
      t.string :link, default: ""
      t.boolean :primary
      t.string :caption, default: ""

      t.timestamps null: false
    end
  end
end
