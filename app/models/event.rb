# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  title        :string
#  description  :text             default("")
#  start_time   :datetime
#  end_time     :datetime
#  venue_id     :integer
#  category_id  :integer
#  organizer_id :integer
#  slug         :string
#  position     :integer
#  accepted     :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Event < ActiveRecord::Base
  has_many :images
  belongs_to :category
  belongs_to :venue
  belongs_to :organizer, :class_name => "User"

  validates_associated :category, :organizer, :venue
  validates :title, :description, :start_time, :end_time, presence: true

  # Named scopes
  scope :after_date, lambda { |date|
    where(["start_time >= ?", date]).order(:start_time)
  }
  scope :on_or_after_date, lambda { |date|
    time = date.beginning_of_day
    where("(events.start_time >= :time) OR (events.end_time IS NOT NULL AND events.end_time > :time)",
      :time => time).order(:start_time)
  }
  scope :before_date, lambda { |date|
    time = date.beginning_of_day
    where("start_time < :time", :time => time).order(start_time: :desc)
  }
  scope :future, lambda { on_or_after_date(Time.zone.today) }
  scope :past, lambda { before_date(Time.zone.today) }
  scope :within_dates, lambda { |start_date, end_date|
    if start_date == end_date
      end_date = end_date + 1.day
    end
    on_or_after_date(start_date).before_date(end_date)
  }
  scope :in_categories, lambda { |categories|
    where(category_id: categories)
  }
  scope :in_regions, lambda { |region|
    joins(:venue).where(:venues => {region_id: region})
  }
end
