class PagesController < ApplicationController

  def index
    @events = Event.all
    @categories = Category.all
  end

end
