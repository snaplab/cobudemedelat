class VenuePolicy
  attr_reader :user, :model

  def initialize(user, model)
    @user = user
    @venue = model
  end

  def index?
    true
  end

  def show?
    true
  end

  def update?
    @user.admin? or @venue.owner == @user if !@user.nil?
  end

  def destroy?
    return false if @user == @venue.owner
    @user.admin?
  end

end
